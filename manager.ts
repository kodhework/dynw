// Distributed Node.js server
// with process instead of clusters, with automatic balancer (using caddy)

import {ModuleInfo, RouteModuleHandler, Server} from './server.ts'

import Os from 'os'
import {Caddy} from 'gitlab://jamesxt94/codes@48904da6/webservers/caddy/server.ts'
import {CaddyHttpRoute, CaddyHttpServer, CaddyRouteMatch} from 'gitlab://jamesxt94/codes@48904da6/webservers/caddy/types.ts'
import Path from 'path'
import fs from 'fs'
import crypto from 'crypto'
//import * as async from "gh+/kwruntime/std@1.1.4/util/async.ts"
import uniqid from "npm://uniqid@5.4.0"
import {Exception} from "gh+/kwruntime/std@a0b97e0/util/exception.ts"
import {AsyncEventEmitter} from "gh+/kwruntime/std@a0b97e0/async/events.ts"
import {Tmux} from "gitlab://jamesxt94/tmux@36df9dca/src/v2/tmux.ts"
import {Server as MeshaServer} from 'gitlab://jamesxt94/mesha@b4efe95a/Server.ts'
import {Client} from 'gitlab://jamesxt94/mesha@b4efe95a/Client.ts'
import {ClientSocket} from 'gitlab://jamesxt94/mesha@b4efe95a/ClientSocket.ts'
import {RPC} from 'gitlab://jamesxt94/mesha@b4efe95a/RPC.ts'

import {parse} from "gitlab://jamesxt94/codes@88af0824/cli-params.ts"
import {kawix} from "github://kwruntime/core@68f0642/src/kwruntime.ts"



export interface RouteStaticHandler{
	path?: string 
	match?: Array<CaddyRouteMatch>
	native: true
	static: {
		root?: string
		path_prefix?: string 
	}
}

export interface RouteNativeHandler{
	native: true
	config?: CaddyHttpRoute
}

export interface RouteModuleNativeHandler extends RouteModuleHandler{
	match?: Array<CaddyRouteMatch>
	native: true
}

export type RouteHandler = RouteStaticHandler | RouteModuleHandler | RouteNativeHandler | RouteModuleNativeHandler


export interface Config extends HostConfig{
	id?: string
	cpus?: number

	startup?: {
		asroot?: boolean
	}
}

export interface HostConfig{
	host: string 
	port?: number
	https_port?: number 
	ssl?: {
		automatic?: boolean 
		key?: string 
		cert?: string 
	}
	upstreams?: [{
		dial: string
	}]
	config?: CaddyHttpServer
}



const defaultConfig = {
	id: 'default',
	cpus: Os.cpus().length,
	host: "127.0.0.1"
}


export class Program{
	static async main(){
		try{
			let cli = parse()
			if(cli.params.cluster !== undefined){
				// start cluster 
				let serv = new Server()
				let addr = await serv.listen(cli.params.address, true)
				console.info("kmux:status:listen:", Buffer.from(JSON.stringify(addr)).toString('base64'),"$$")
				console.info("Server listen on:", addr)

				let server = new MeshaServer()
				server.on("error", console.error)
				server.on("client", (socket : ClientSocket)=>{
					let rpc = new RPC()
					rpc.channel = socket
					rpc.init()
					rpc.defaultScope.add("server", serv)
					socket.on("close", ()=>{
						rpc = null
					})
				})
				if(cli.params.dynwid){
					let client = await Client.connectLocal(cli.params.dynwid)
					const rpc = new RPC()
					rpc.channel = client  
					rpc.init()	
					let manager = await rpc.defaultScope.remote("manager")
					serv.master = manager

					await manager.$addCluster(cli.params.id, serv)
				}

				
				await serv.init()
			}
		}catch(e){
			console.error("> Failed to execute:", e.message)
		}
	}
}




export class Router {
	#manager: Manager
	#routes: Array<RouteHandler> = []
	#defaults: Array<RouteHandler> = []
	#native: Array<RouteHandler> = []

	constructor(m:Manager){
		this.#manager = m 
	}

	


	get native(){
		return this.#native
	}

	add(handler: RouteHandler){
		if(handler["native"]){
			this.#native.push(handler)
			if(handler["module"]){
				handler["moduleId"] = uniqid()
				this.#routes.push(handler)
				this.#propagate()	
			}
			this.#manager.updateChanges()
		}
		else{
			this.#routes.push(handler)
			this.#propagate()
		}
		
	}

	addDefault(handler: RouteHandler){
		this.#defaults.push(handler)
		this.#propagate()		
	}

	remove(handler: RouteHandler){
		let i = this.#routes.indexOf(handler)
		let ok = false
		if(i >= 0){
			this.#routes.splice(i,1)
			ok = true 
		}
		else{
			i = this.#native.indexOf(handler)
			if(i >= 0){
				this.#native.splice(i,1)
				ok = true 
			}
			else{
				i = this.#defaults.indexOf(handler)
				if(i >= 0){
					this.#defaults.splice(i,1)
					ok = true 
				}	
			}
		}

		if(ok) this.#propagate()		
	}

	async #propagate(){
		if(this.#propagate["timer"]){
			clearTimeout(this.#propagate["timer"])
		}
		this.#propagate["timer"] = setTimeout(this.updateChanges.bind(this), 200)
	}

	

	async updateChanges(server?: Server){
		try{
			if(this.#propagate["timer"]){
				clearTimeout(this.#propagate["timer"])
			}
			let servers = this.#manager.servers 
			let keys = servers.keys()
			for(let key of keys){
				await this.$updateServer(servers.get(key))
			}
		}catch(e){
			let ex = Exception.create("Failed propagate routes: " + e.message, e).putCode("ROUTER_PROPAGATE_ERROR")
			this.#manager.emit("error", ex)
		}
	}

	protected async $updateServer(server?: Server){
		await server.setRoutes([...this.#routes, ...this.#defaults])
	}
}

export class Manager extends AsyncEventEmitter{

	#config: Config 
	#socket_addresses : Array<string>
	#rid : string 
	#cid : string 
	#caddy: Caddy
	#servers = new Map<string, Server>()
	#serverHandler : ModuleInfo 
	#startupHandler: ModuleInfo
	#router : Router
	#hosts = new Set<HostConfig>()
	#defaultRoute:any 


	startup = {
		url: '',
		method: ''
	}





	static getLocalId(id: string = 'default'){
		if((id.indexOf("/") >= 0) || (id.indexOf(":") >= 0)){
			id = crypto.createHash('md5').update(id).digest('hex')
		}
		let uid = "dynw-" + id
		let getId = (id: string) => `${process.env.USER}.kmux-${id}`
		return {
			id,
			uid,
			meshaId: getId(uid)
		}

	}


	constructor(config: Config){
		super()
		this.#config = Object.assign(defaultConfig, config)
		let res = Manager.getLocalId(this.#config.id)

		
		this.#cid = res.uid
		this.#caddy = new Caddy(this.#cid)
		this.#caddy.config.admin = {
			disabled: true
		}
		this.#rid = res.meshaId 
		
		if(Os.platform() == "win32"){
			// bind on TCP Ports, because named pipes are limited
			this.#socket_addresses = []
			for(let i=0;i<this.#config.cpus;i++){
				let num = parseInt(String(Math.random() * 200)) + 2
				this.#socket_addresses.push(`tcp://127.0.0.${num}:0`)
			}
		}
		else{

			// bind on unix sockets
			let folder = Path.join(Os.homedir(), ".kawi", "user-data")
			if(!fs.existsSync(folder)){
				fs.mkdirSync(folder)
			}
			folder = Path.join(folder, "com.kodhe.dynw")
			if(!fs.existsSync(folder)){
				fs.mkdirSync(folder)
			}

			folder = Path.join(folder, "sockets")
			if(!fs.existsSync(folder)){
				fs.mkdirSync(folder)
			}
			
			folder = Path.join(folder, res.id)
			if(!fs.existsSync(folder)){
				fs.mkdirSync(folder)
			}
			
			this.#socket_addresses = []
			for(let i=0;i<this.#config.cpus;i++){
				this.#socket_addresses.push(`unix://${folder}/${i}.socket`)
			}			
		}
	}

	get config(){
		return this.#config
	}


	get hosts(){
		return this.#hosts
	}

	get servers(){
		return this.#servers
	}

	get router(){
		if(!this.#router){
			this.#router = new Router(this)
		}
		return this.#router
	}

	get caddy(){
		return this.#caddy
	}

	

	async setCustomHandler(handler: ModuleInfo){
		this.#serverHandler = handler
		let keys = this.#servers.keys()
		for(let key of keys){
			await this.#servers.get(key).setCustomHandler(handler)
		}
	}

	async setStartupHandler(handler: ModuleInfo){
		this.#startupHandler = handler
		let keys = this.#servers.keys()
		for(let key of keys){
			await this.#servers.get(key).setStartupHandler(handler)
		}
	}


	async $addCluster(id: string, server: Server){
		this.#servers.set(id, server)
		if(this.#serverHandler){
			await server.setCustomHandler(this.#serverHandler)
		}
		if(this.#startupHandler){
			await server.setStartupHandler(this.#startupHandler)
		}
		if(this.#router){
			this.#router["$updateServer"](server)
		}
	}


	#processConfig(config: HostConfig, parent?: HostConfig){

		let hconfig = Object.assign({}, parent||{}, config)
		if(hconfig.config)
			return hconfig.config


		let ports = []
		if(hconfig.port) ports.push(hconfig.port)
		if(hconfig.https_port) ports.push(hconfig.https_port)
		const listen = ports.map((a) => ":" + a)

		let tls_connection_policies = null 
		if(hconfig.ssl && !hconfig.ssl.automatic){

			if(!hconfig.ssl["uid"]){
				Object.defineProperty(hconfig.ssl, "uid",{
					value: uniqid(),
					enumerable: false
				})
			}

			tls_connection_policies = [
				{
					"certificate_selection": {
						"any_tag": [hconfig.ssl["uid"]]
					}
				}
			]

			let tls = this.#caddy.config.apps["tls"]
			if(!tls){
				tls = {
					certificates: {
						load_files: []
					}
				}
				this.#caddy.config.apps["tls"] = tls
			}
			let certconfig = tls.certificates.load_files.filter((a) => a.tags[0] == hconfig.ssl["uid"])[0]
			if(!certconfig){
				certconfig = {
					certificate: '',
					key: '',
					format:'pem',
					tags: [hconfig.ssl["uid"]]
				}
				tls.certificates.load_files.push(certconfig)
			}

			certconfig.certificate = hconfig.ssl.cert
			certconfig.key = hconfig.ssl.key
			
		}

		
		let defRoute = Object.assign({}, this.#defaultRoute)
		if(hconfig.upstreams){
			defRoute.handle = [defRoute.handle[0]]
			defRoute.handle[0] = Object.assign({}, defRoute.handle[0])
			defRoute.handle[0].upstreams = hconfig.upstreams
		}
		let rconfig = {
			listen,
			routes: [
				defRoute
			],
			tls_connection_policies
		}
		return rconfig
	}

	async startServer(){
		let client = await this.#connectTmux()

		
		let upstreams:any = []
		if(Os.platform() == "win32"){
		}
		else{
			upstreams = this.#socket_addresses.map((a) => ({dial: "unix/" + a.substring(7)}))
		}


		this.#defaultRoute = {
			handle: [
				{
					handler: "reverse_proxy",
					upstreams,
					headers: {
						request: {
							set: {
								"caddy-web-url": ["{http.request.uri}"]
							}
						}
					}
				}				
			]
			
		}

		this.#caddy.config.apps = {
			http: {
				http_port: this.#config.port,
				https_port: this.#config.https_port,
				servers: {
				}
			}
		}
		this.#caddy.config.apps.http.servers[this.#config.host] = this.#processConfig(this.#config)


		client.on("status:listen", (event) => {
			let id = event.id 
			let i = Number(id.substring(2))
			let addrBase64 = event.data
			let addr = JSON.parse(Buffer.from(addrBase64,'base64').toString())
			this.emit("cluster-address", {
				id,
				address: addr,
				number: i
			})
			if(addr.port){
				upstreams[i] = {
					dial: `${addr.address}:${addr.port}`
				}
				this.updateChanges()
			}
		})


		let createProcess = async (i: number) => {
			let proid = `ws${i}`
			let file = import.meta.url
			let startupArgs = []
			if(this.startup.url && this.startup.method){
				startupArgs.push("--startup-url=" + this.startup.url)
				startupArgs.push("--startup-method=" + this.startup.method)
			}
			let pro = await client.createProcess({
				id: proid,
				cmd: process.argv[0],
				args: [kawix.filename, file, "--dynwid=" + this.#rid, "--cluster", "--id=" + proid, "--address=" + this.#socket_addresses[i], ...startupArgs],
				env: process.env
			})
			await pro.start()
			//await pro.start(process.argv[0], )
		}

		for(let i=0;i<this.#config.cpus;i++){
			let pro = await client.getProcess(`ws${i}`)
			if(pro){
				await client.delete(pro.info.id)
			}
			await createProcess(i)
		}

		this.emit("clusters-started")
		
		let cmd = await this.#caddy.getCmd()
		//let pro = await client.createProcess("caddy")
		if(this.#config.startup?.asroot){
			cmd.args = [cmd.bin, ...cmd.args]
			cmd.bin = "sudo"
		}

		let pro = await client.createProcess({
			id: "caddy",
			cmd: cmd.bin,
			args: cmd.args,
			env: process.env
		})
		await pro.start()
		
		//await pro.start(cmd.bin, cmd.args)
		this.emit("caddy-started")
	}

	async updateChanges(){
		if(this.updateChanges["timer"]){
			clearTimeout(this.updateChanges["timer"])
		}
		this.updateChanges["timer"] = setTimeout(async ()=> {
			try{

				this.#caddy.config.apps.http.servers = {}
				let defaultserver = this.#processConfig(this.#config)
				
			
				if(this.#router){
					let route : any 
					defaultserver.routes = []
					for(let routeHandler of this.#router.native){
						if(routeHandler["path"]){
							routeHandler["match"] = [
								{
									path: [routeHandler["path"]]
								}
							]
						}
						let h = routeHandler as RouteStaticHandler
						if(h.static){
							if(h.path && (h.path.indexOf("*") <= 0)){
								routeHandler["match"] = [
									{
										path: [h.path + "/*"]
									}
								]
							}
							let path_prefix = h.static.path_prefix
							if(!path_prefix && h.path){
								path_prefix = h.path
								if(path_prefix.endsWith("*")){
									path_prefix = Path.posix.dirname(path_prefix)
								}
							}
							route = {
								match: h.match,
								handle: [
									{
										handler:'rewrite',
										strip_path_prefix: path_prefix
									},
									{
										handler:'file_server',
										root: h.static.root
									}
								]
							}
							defaultserver.routes.push(route)
						}
						else{
							if(routeHandler["config"]){
								defaultserver.routes.push(routeHandler["config"])
							}
							else if(routeHandler["module"]){
								route = {
									match: routeHandler["match"],
									handle: [
										{
											handler:'reverse_proxy',
											upstreams: this.#defaultRoute.handle[0].upstreams,
											headers: {
												request: {
													set: {
														"caddy-web-url": ["{http.request.uri}"]
													},
													add: {
														"module-uid": [routeHandler["moduleId"]]
													}
												}
											}
										}
									]
								}
								defaultserver.routes.push(route)
							}

						}
					}
					defaultserver.routes.push(this.#defaultRoute)
				}

				for(let hostConfig of this.#hosts){					
					let c = this.#caddy.config.apps.http.servers[hostConfig.host] = this.#processConfig(hostConfig, this.#config)
					if(!c.routes){
						c.routes = defaultserver.routes
					}
				}
				this.#caddy.config.apps.http.servers[this.#config.host] = defaultserver
				delete this.updateChanges["timer"]
				await this.#caddy.update()
			}catch(e){
				this.emit("caddy-error", e)
			}
		}, 100)
	}


	async #connectTmux(){
		let tmuxid = this.#rid
		
		this.emit("tmux-check")
	
		
		// this starts a local server
		let client = new Tmux(tmuxid)
		/*
		if(await client.checkLocalServer()){
			throw Exception.create(`Tmux client with id ${tmuxid} yet started`).putCode("TMUX_STARTED")
		}
		*/

		

		let server = new MeshaServer()
		server.on("error", console.error)
		server.on("client", (socket : ClientSocket)=>{
			let rpc = new RPC()
			rpc.channel = socket
			rpc.init()
			rpc.defaultScope.add("getObject", () => client)
			rpc.defaultScope.add("manager", this)
			socket.on("close", ()=>{
				//console.info("Closing client. Current scopes length:", rpc.$scopes.size)
				rpc = null
			})
		})		
		await client.init()

		
		await server.startLocal(tmuxid)
		this.emit("tmux-started")

		let onexit = async() => {
			console.info('> Closing process')
			try{ await client.close() }catch(e){}
			process.exit()
		}
		process.on("SIGINT", onexit)
		process.on("SIGTERM", onexit)
		process.on("SIGQUIT", onexit)		
		return client 
	}

}